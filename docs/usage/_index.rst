.. _usage:
Usage
=====
Define a configuration in ``config.yaml``

.. code-block:: console

    # config.yaml
    IsolationForest_0.01:
        project_name: timeseries_anomaly
        run_this_project: True
        multi_variate_model: True
        model: IsolationForest
        data:
            path: 'path-to-train-data.csv'
            test_data_path: 'path-to-train-data.csv'
            features_to_avoid: ['feat-to-avoid']
        hyperparams:
            contamination: 0.01
        results:
            path: 'results/isolation_forest_001/'
        remote_run: False


Run the model defined in ``config.yaml``

.. code-block:: console

    # example_script.py
    import ai4ao # import package 
    from ai4ao.models import SKLearnModel as Model # scikit-learn wrapper 

    # fit and evaluate model
    model = Model(plot_results=True)
    model.batch_fit(path_config='configs.yaml')

    # print models and metrics
    print(model.models)
    print(model.metrics())

.. toctree::
   :caption: Usage
