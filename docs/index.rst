.. AI4AO documentation master file, created by
   sphinx-quickstart on Wed Jun  9 18:35:25 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. useful examples on html theme: https://opencolorio.readthedocs.io/en/latest/index.html
.. useful examples on html theme: https://ktl.leftxs.org/install.html


.. _welcome:
AI for Anomaly and Outlier Detection: AI4AO!
============================================================

`AI for Anomaly and Outlier detection (AI4AO) <https://pypi.org/project/ai4ao>`_ is a Python package that allows to build any of the 
`scikit-learn <https://scikit-learn.org/stable/>`_ supported Clustering and Classification algorithms
based machine learning models in batches. This means that one can use `yaml <https://yaml.org>`_ declarative
syntax in order to write a configuration file, and based on the instructions in the configuration file, and the 
machine learning models will be constructed sequentially. This way many models can be built with a single configuration 
file with the results being arranged in an extremely modular way. AI4AO can be considered as a convenient wrapper for
scikit-learn models.

.. note::

    * Developed by: **Selvakumar Ulaganathan**
    * Website: `www.selvai.com <https://www.selvai.com>`_
    * PyPi: `Project Homepage <https://pypi.org/project/ai4ao>`_
    * GitLab: `Source Code <https://gitlab.com/selvai/ai4ao>`_

.. toctree::
   :hidden:

   main_features/_index

.. toctree::
   :hidden:

   installation/_index

.. toctree::
   :hidden:

   usage/_index

.. toctree::
   :hidden:

   examples/_index


.. toctree::
   :hidden:

   api/_index
