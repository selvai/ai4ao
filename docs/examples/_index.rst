.. _examples:

Examples
========
Example scripts can be obtained from `GitLab Source Code <https://scikit-learn.org/stable/>`_!

.. toctree::
   :caption: Examples