.. _main_features:
Main Features
=============
* single template or configuration file
    - allows one to define various configurations
    - declaratively specify various parameters
        - the (scikit-learn) algorithm and its hyper-parameters to run
        - location of the training and testing data
        - location of the results to be generated
        - define a configuration with an ability to either run or use it as a template
* ability to build multiple model sequentially in a simple way
    - not using multi-processing/threading
* esay to extend to other scikit-learn or other framework based models
    - current version supports:
        * `Isolation Forest <https://scikit-learn.org/stable/>`_ 
        * `Elliptic Envelope <https://scikit-learn.org/stable/>`_  
        * `One class SVM <https://scikit-learn.org/stable/>`_ 
        * `Local Outlier Factor <https://scikit-learn.org/stable/>`_ 

.. toctree::
   :caption: Main Features
