.. _api:

API
===

.. note::

    All the ``functions`` can be seen in :ref:`genindex`!


.. note::

    All the ``modules`` can be seen in :ref:`modindex`!


.. note::

    Search for the relevant ``utilities`` in :ref:`search`!


.. toctree::
   :caption: API