.. _installation:
Installation
============
Install via ``pip``:

.. code-block:: console

    pip install ai4ao


Install in editable mode using ``GitLab`` source:

.. code-block:: console

    git clone https://gitlab.com/selvai/ai4ao.git
    cd ai4ao
    pip install -e .

.. toctree::
   :caption: Installation
