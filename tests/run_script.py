import logging

import ai4ao
from ai4ao.models import SKLearnModel as Model

# set logging
logger = logging.getLogger()
handler = logging.StreamHandler()
formatter = logging.Formatter(
    '%(asctime)s %(name)-12s %(levelname)-8s %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(logging.DEBUG)
logging.getLogger('matplotlib').setLevel(logging.WARNING)



# fit and infer from model
model = Model(plot_results=True)
model.batch_fit(path_config='configs.yaml')
logging.info(model.models)
logging.info(model.metrics())
